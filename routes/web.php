<?php

use App\Http\Controllers\Admin\SupportChatController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    \App\Models\User::create([
        'name' => 'assyl',
        'email' => 'admin@admin.ru',
        'password' => '11072000a',
        'phone' => '+77020202',
        'surname' => 'surname',
        'role_id' => 1,
    ]);

    return view('welcome');
});

Route::get('/apple-app-site-association', function () {
    $json = file_get_contents(base_path('.well-known/apple-app-site-association'));
    return response($json, 200)
        ->header('Content-Type', 'application/json');
});

Route::resource('/messages', SupportChatController::class);
