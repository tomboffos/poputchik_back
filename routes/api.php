<?php

use App\Http\Controllers\Api\Auth\LogoutController;
use App\Http\Controllers\Api\Driver\Auth\AuthDriverController;
use App\Http\Controllers\Api\Driver\DriverController;
use App\Http\Controllers\Api\Driver\OrderController;
use App\Http\Controllers\Api\General\ChatController;
use App\Http\Controllers\Api\General\HelpPhoneController;
use App\Http\Controllers\Api\General\TrackController;
use App\Http\Controllers\Api\User\Auth\AuthController;
use App\Http\Controllers\Api\User\OrderRequestController;
use App\Http\Controllers\Api\User\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('user')->group(function () {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/check/{user}', [AuthController::class, 'check']);
});

Route::prefix('driver')->group(function () {
    Route::post('/register', [AuthDriverController::class, 'register']);
    Route::post('/check/{user}', [AuthDriverController::class, 'check']);
});


Route::middleware('auth:sanctum')->group(function () {
    Route::resources([
        '/user' => UserController::class,
        '/orders' => OrderController::class,
        '/tracks' => TrackController::class,
        '/order-requests' => OrderRequestController::class,
        '/help-phones' => HelpPhoneController::class,
        '/chat' => ChatController::class
    ]);

    Route::post('/rate/{driver}', [DriverController::class, 'addRating']);
    Route::post('/rate-user/{user}', [UserController::class, 'rate']);

    Route::post('/balance', [DriverController::class, 'fillBalance']);
    Route::post('/driver/update', [DriverController::class, 'update']);
    Route::post('/driver/become', [DriverController::class, 'become']);
    Route::post('/user/update', [UserController::class, 'update']);
    Route::post('/logout', [LogoutController::class, 'logout']);

    Route::post('/cancel-order/{order}', [OrderController::class, 'cancelOrder']);
});
Route::post('/orders/socket', [OrderController::class, 'socket']);
