<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->postJson('/api/user/register', [
            'phone' => '+7 (707) 303-00-10',
            'password' => '11072000a',
            'surname' => 'Tasbulatov',
            'name' => 'asyl',
            'token' => '122'
        ]);

        $response->assertStatus(200);
    }
}
