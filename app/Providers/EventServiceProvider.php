<?php

namespace App\Providers;

use App\Models\Message;
use App\Models\Order;
use App\Models\OrderRequest;
use App\Models\Track;
use App\Models\User;
use App\Observers\MessageObserver;
use App\Observers\OrderObserver;
use App\Observers\OrderRequestObserver;
use App\Observers\TrackObserver;
use App\Observers\UserObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
        Message::observe(MessageObserver::class);
        OrderRequest::observe(OrderRequestObserver::class);
        Order::observe(OrderObserver::class);
        Track::observe(TrackObserver::class);
        User::observe(UserObserver::class);
    }
}
