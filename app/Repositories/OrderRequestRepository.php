<?php


namespace App\Repositories;


use App\Http\Requests\StoreOrderRequestRequest;
use App\Http\Requests\UpdateOrderRequestRequest;
use App\Models\OrderRequest;
use App\Repositories\Interfaces\OrderRequestInterface;

class OrderRequestRepository implements OrderRequestInterface
{

    public function create(StoreOrderRequestRequest $request)
    {
        return OrderRequest::create(array_merge($request->validated(), [
            'user_id' => $request->user()->id,
            'order_request_status_id' => 1
        ]));
    }

    public function update(UpdateOrderRequestRequest $request, OrderRequest $orderRequest)
    {
        $orderRequest->update($request->all());
        return $orderRequest;
    }
}
