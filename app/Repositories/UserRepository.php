<?php


namespace App\Repositories;


use App\Http\Requests\Api\User\Auth\RegisterRequest;
use App\Http\Requests\AuthCheckCodeRequest;
use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Services\DeviceService;

class UserRepository implements UserRepositoryInterface
{
    private $deviceService;


    public function __construct()
    {
        $this->deviceService = new DeviceService();
    }

    public function create(RegisterRequest $data)
    {

        $user = User::create(array_merge($data->except('token'), [
            'role_id' => 1
        ]));

        $this->deviceService->insertDevice($data['token'], $user);

        return $user;
    }

    public function update(User $user, $data)
    {
        // TODO: Implement update() method.
    }

    public function check(AuthCheckCodeRequest $request, User $user): ?User
    {
        if ($user->code == $request->code)
            return $user;

        return null;
    }

    public function rate(User $user, int $rating): User
    {
        $user->update([
            'rate' => $user->rate + $rating,
            'rate_count' => $user->rate_count + 1
        ]);

        return $user;
    }
}
