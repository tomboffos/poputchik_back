<?php


namespace App\Repositories;


use App\Http\Requests\Admin\SupportStoreMessageRequest;
use App\Http\Requests\StoreMessageRequest;
use App\Models\Message;
use App\Repositories\Interfaces\MessageRepositoryInterface;

class MessageRepository implements MessageRepositoryInterface
{

    public function create(StoreMessageRequest $request)
    {
        return Message::create(array_merge($request->validated(), ['user_id' => $request->user()->id, 'sender_id' => $request->user()->id]));
    }

    public function createBySupport(SupportStoreMessageRequest $request)
    {
        return Message::create(array_merge($request->validated(), ['support_id' => backpack_user()->id, 'sender_id' => backpack_user()->id]));
    }
}
