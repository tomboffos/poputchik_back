<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Api\Driver\Auth\AuthDriverRequest;
use App\Http\Requests\AuthCheckCodeRequest;
use App\Models\User;

interface DriverRepositoryInterface
{
    public function create(AuthDriverRequest $request);

    public function checkDriverCode(AuthCheckCodeRequest $request, User $user) : ?User;
}
