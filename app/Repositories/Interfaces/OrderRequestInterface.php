<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\StoreOrderRequestRequest;
use App\Http\Requests\UpdateOrderRequestRequest;
use App\Models\OrderRequest;

interface OrderRequestInterface
{
    public function create(StoreOrderRequestRequest $request);

    public function update(UpdateOrderRequestRequest $request, OrderRequest $orderRequest);
}
