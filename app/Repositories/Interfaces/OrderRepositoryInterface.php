<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Order;

interface OrderRepositoryInterface
{
    public function create(StoreOrderRequest $request);

    public function update(UpdateOrderRequest $request,Order $order);
}
