<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Api\User\Auth\RegisterRequest;
use App\Http\Requests\AuthCheckCodeRequest;
use App\Models\User;

interface UserRepositoryInterface
{

    public function create(RegisterRequest $data);

    public function update(User $user, $data);

    public function check(AuthCheckCodeRequest $request, User $user) : ?User;

    public function rate(User $user, int $rating) : User;

}
