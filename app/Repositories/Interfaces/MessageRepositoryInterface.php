<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Admin\SupportStoreMessageRequest;
use App\Http\Requests\StoreMessageRequest;
use App\Models\Message;
use Illuminate\Http\Request;

interface MessageRepositoryInterface
{
    public function create(StoreMessageRequest $request);

    public function createBySupport(SupportStoreMessageRequest $request);
}
