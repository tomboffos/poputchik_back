<?php


namespace App\Repositories;


use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Order;
use App\Repositories\Interfaces\OrderRepositoryInterface;

class OrderRepository implements OrderRepositoryInterface
{

    public function create(StoreOrderRequest $request)
    {
        return Order::create(array_merge($request->validated(), [
            'order_status_id' => 1,
            'driver_id' => $request->user()->driver->id
        ]));
    }

    public function update(UpdateOrderRequest $request, Order $order)
    {
        $order->update($request->all());



        return $order;
    }
}
