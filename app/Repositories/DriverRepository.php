<?php


namespace App\Repositories;


use App\Http\Requests\Api\Driver\Auth\AuthDriverRequest;
use App\Http\Requests\AuthCheckCodeRequest;
use App\Http\Requests\BecomeDriverRequest;
use App\Models\Driver;
use App\Models\User;
use App\Repositories\Interfaces\DriverRepositoryInterface;
use App\Services\DeviceService;

class DriverRepository implements DriverRepositoryInterface
{
    /**
     * @var DeviceService
     */
    private $deviceService;

    public function __construct()
    {
        $this->deviceService = new DeviceService();
    }

    public function create(AuthDriverRequest $request)
    {
        $user = User::create([
            'name' => $request['name'],
            'surname' => $request['surname'],
            'password' => $request['password'],
            'phone' => $request['phone'],
            'role_id' => 2
        ]);

        Driver::create([
            'iin' => $request['iin'],
            'car_model' => $request['car_model'],
            'car_color' => $request['car_color'],
            'car_id' => $request['car_id'],
            'car_year' => $request['car_year'],
            'driver_id_images' => $request['driver_id_images'],
            'images' => $request['images'],
            'user_id' => $user->id,
            'seats' => $request['seats']
        ]);

        $this->deviceService->insertDevice($request->token, $user);

        return $user;

    }

    public function becomeDriver(BecomeDriverRequest $request)
    {
        Driver::create([
            'iin' => $request['iin'],
            'car_model' => $request['car_model'],
            'car_color' => $request['car_color'],
            'car_id' => $request['car_id'],
            'car_year' => $request['car_year'],
            'driver_id_images' => $request['driver_id_images'],
            'images' => $request['images'],
            'user_id' => $request->user()->id,
            'seats' => $request['seats']
        ]);

        return $request->user();

    }


    public function checkDriverCode(AuthCheckCodeRequest $request, User $user) : ?User
    {
        if ($user->code == $request->code) {
            return $user;
        }

        return null;
    }
}
