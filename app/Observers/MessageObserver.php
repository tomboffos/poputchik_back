<?php

namespace App\Observers;

use App\Http\Resources\ChatResource;
use App\Models\Message;
use App\Services\FirebaseService;
use App\Services\WebSocket;

class MessageObserver
{
    /**
     * Handle the Message "created" event.
     *
     * @param \App\Models\Message $message
     * @return void
     */
    public function created(Message $message)
    {
        //
        WebSocket::sendEvent("poputchik.message_sent.{$message->user_id}.{$message->support_id}", new ChatResource($message));
        WebSocket::sendEvent("poputchik.message.sent.{$message->user_id}",new ChatResource($message));
        if ($message->sender_id != $message->user_id)
            FirebaseService::send($message->user->devices(), 'Вам пришло сообщение',$message->message_content->text);

    }

    /**
     * Handle the Message "updated" event.
     *
     * @param \App\Models\Message $message
     * @return void
     */
    public function updated(Message $message)
    {
        //
    }

    /**
     * Handle the Message "deleted" event.
     *
     * @param \App\Models\Message $message
     * @return void
     */
    public function deleted(Message $message)
    {
        //
    }

    /**
     * Handle the Message "restored" event.
     *
     * @param \App\Models\Message $message
     * @return void
     */
    public function restored(Message $message)
    {
        //
    }

    /**
     * Handle the Message "force deleted" event.
     *
     * @param \App\Models\Message $message
     * @return void
     */
    public function forceDeleted(Message $message)
    {
        //
    }
}
