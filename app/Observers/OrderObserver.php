<?php

namespace App\Observers;

use App\Models\Order;
use App\Services\OrderService;
use App\Services\WebSocket;

class OrderObserver
{
    /**
     * Handle the Order "created" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function created(Order $order)
    {
        //
        $service = new OrderService();

        $service->searchForOrders($order);
    }

    /**
     * Handle the Order "updated" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function updated(Order $order)
    {
        if ($order->getOriginal('order_status_id') != $order->order_status_id) {
            if ($order->order_status_id == 3) {
                $order->update([
                    'price' => $order->completedRequest()->sum('price'),
                    'commission' => $order->completedRequest()->sum('price') * 0.05
                ]);
            }



        }

    }

    /**
     * Handle the Order "deleted" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function deleted(Order $order)
    {
        //
    }

    /**
     * Handle the Order "restored" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function restored(Order $order)
    {
        //
    }

    /**
     * Handle the Order "force deleted" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function forceDeleted(Order $order)
    {
        //
    }
}
