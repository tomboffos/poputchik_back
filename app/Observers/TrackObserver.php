<?php

namespace App\Observers;

use App\Models\Track;

class TrackObserver
{
    /**
     * Handle the Track "created" event.
     *
     * @param \App\Models\Track $track
     * @return void
     */
    public function created(Track $track)
    {
        //
        $newTrack = new Track();
        $newTrack->start_name = $track->end_name;
        $newTrack->end_name = $track->start_name;
        $newTrack->points = json_encode(array_reverse(json_decode($track->points)));
        $newTrack->start_price = $track->start_price;
        $newTrack->start_price_comfort = $track->start_price_comfort;
        $newTrack->custom_price = $track->custom_price;
        $newTrack->prices = $track->prices;
        $newTrack->saveQuietly();


    }

    /**
     * Handle the Track "updated" event.
     *
     * @param \App\Models\Track $track
     * @return void
     */
    public function updated(Track $track)
    {
        //


        Track::where('start_name', $track->getOriginal('end_name'))->where('end_name', $track->getOriginal('start_name'))
            ->first()
            ->updateQuietly([
                'points' => json_encode(array_reverse(json_decode($track->points))),
                'start_price' => $track->start_price,
                'start_price_comfort' => $track->start_price_comfort,
                'custom_price' => $track->custom_price,
                'prices' => $track->prices
            ]);
    }

    /**
     * Handle the Track "deleted" event.
     *
     * @param \App\Models\Track $track
     * @return void
     */
    public function deleted(Track $track)
    {
        //
    }

    /**
     * Handle the Track "restored" event.
     *
     * @param \App\Models\Track $track
     * @return void
     */
    public function restored(Track $track)
    {
        //
    }

    /**
     * Handle the Track "force deleted" event.
     *
     * @param \App\Models\Track $track
     * @return void
     */
    public function forceDeleted(Track $track)
    {
        //
    }
}
