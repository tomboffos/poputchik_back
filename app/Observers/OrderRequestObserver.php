<?php

namespace App\Observers;

use App\Http\Resources\OrderRequestResource;
use App\Http\Resources\OrderResource;
use App\Models\OrderRequest;
use App\Services\FirebaseService;
use App\Services\OrderRequestService;
use App\Services\WebSocket;

class OrderRequestObserver
{
    /**
     * Handle the OrderRequest "created" event.
     *
     * @param \App\Models\OrderRequest $orderRequest
     * @return void
     */
    public function created(OrderRequest $orderRequest)
    {
        //
        $service = new OrderRequestService();

        $service->checkOrders($orderRequest);
    }

    /**
     * Handle the OrderRequest "updated" event.
     *
     * @param \App\Models\OrderRequest $orderRequest
     * @return void
     */
    public function updated(OrderRequest $orderRequest)
    {
        //
        if ($orderRequest->getOriginal('order_id') != $orderRequest->order_id &&
            $orderRequest->getOriginal('order_request_status_id') != $orderRequest->order_request_status_id &&
            $orderRequest->order_request_status_id == 2) {
            FirebaseService::send($orderRequest->user->devices(), 'Водитель найден', "{$orderRequest->order->driver->user->name} заберет вас");
            FirebaseService::send($orderRequest->order->driver->user->devices(), 'Новый заказ', "{$orderRequest->user->name} предлагает вам {$orderRequest->price} тг");

        }
        if ($orderRequest->getOriginal('order_request_status_id') != $orderRequest->order_request_status_id && $orderRequest->order_request_status_id == 4) {
            WebSocket::sendEvent('poputchik.order.request.deleted.' . $orderRequest->order->id, new OrderResource($orderRequest->order));
            FirebaseService::send($orderRequest->order->driver->user->devices(), 'Отменили заказ', "{$orderRequest->user->name} отменил текущий заказ");

        }
        if ($orderRequest->getOriginal('order_request_status_id') != $orderRequest->order_request_status_id && $orderRequest->order_request_status_id == 2) {
            WebSocket::sendEvent('poputchik.order.request.accepted.' . $orderRequest->order->id, new OrderResource($orderRequest->order));
            WebSocket::sendEvent('poputchik.order.request.accepted.' . $orderRequest->id, new OrderRequestResource($orderRequest));

        }

        if ($orderRequest->getOriginal('order_request_status_id') != $orderRequest->order_request_status_id) {
            if ($orderRequest->order_request_status_id == 5){
                FirebaseService::send($orderRequest->order->driver->user->devices(), 'Новый заказ', "{$orderRequest->user->name} предлагает вам {$orderRequest->price} тг");
                WebSocket::sendEvent('poputchik.order.request.sent.' . $orderRequest->order->id, new OrderResource($orderRequest->order));
            }

            if ($orderRequest->order_request_status_id == 7) {
                WebSocket::sendEvent('poputchik.order.request.arrived.' . $orderRequest->order->id, new OrderResource($orderRequest->order));
                WebSocket::sendEvent('poputchik.order.request.arrived.' . $orderRequest->id, new OrderResource($orderRequest->order));

                FirebaseService::send($orderRequest->user->devices(), 'Водитель приехал у вас есть 3 минуты!', "{$orderRequest->order->driver->user->name} приехал");
            }

            if ($orderRequest->order_request_status_id == 3){
                WebSocket::sendEvent('poputchik.order.request.deleted.' . $orderRequest->id, new OrderResource($orderRequest->order));
                FirebaseService::send($orderRequest->order->user->devices(), 'Отменили заказ', "{$orderRequest->order->driver->user->name} отменил текущий заказ");
            }

            if ($orderRequest->order_request_status_id == 8){
                WebSocket::sendEvent('poputchik.order.request.ended.' . $orderRequest->id, new OrderResource($orderRequest->order));

            }

            if ($orderRequest->order_request_status_id == 9){
                WebSocket::sendEvent('poputchik.order.request.get.' . $orderRequest->id, new OrderResource($orderRequest->order));

            }
        }


    }

    /**
     * Handle the OrderRequest "deleted" event.
     *
     * @param \App\Models\OrderRequest $orderRequest
     * @return void
     */
    public function deleted(OrderRequest $orderRequest)
    {
        //
    }

    /**
     * Handle the OrderRequest "restored" event.
     *
     * @param \App\Models\OrderRequest $orderRequest
     * @return void
     */
    public function restored(OrderRequest $orderRequest)
    {
        //
    }

    /**
     * Handle the OrderRequest "force deleted" event.
     *
     * @param \App\Models\OrderRequest $orderRequest
     * @return void
     */
    public function forceDeleted(OrderRequest $orderRequest)
    {
        //
    }
}
