<?php


namespace App\Services;


use App\Models\Order;
use App\Models\OrderRequest;
use Illuminate\Support\Facades\Log;

class OrderRequestService
{

    public function checkOrders(OrderRequest $orderRequest)
    {
        $orders = Order::where('track_id', $orderRequest->track_id)->where('order_status_id', 1)->orderBy('id', 'desc')->get();
        foreach ($orders as $order) {
            Log::info('order'. $order->id);

            if (count($order->requests()) < $order->seats)
                $orderRequest->update([
                    'order_id' => $order->id,
                    'order_request_status_id' => $order->automated ? 2 : 5,
                ]);
        }
    }
}
