<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CodeService
{
    public static function addCode(User $user)
    {
        $user->update([
            'code' => (string)mt_rand(1000, 9999)
        ]);

        try {
            Mail::send('mails.code', ['user' => $user],function($m) use ($user){
                $m->from(env('MAIL_FROM_ADDRESS'), 'Регистрационный код');

                $m->to($user->email, $user->name)->subject('Регистрационный код');
            });
        } catch (\Exception $e) {
            Log::error('check '. $e->getMessage());
        }
    }
}
