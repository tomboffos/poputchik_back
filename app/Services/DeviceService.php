<?php


namespace App\Services;


use App\Models\User;
use App\Models\UserDevice;

class DeviceService
{
    public function insertDevice($token, User $user)
    {
        if (!$user->checkIfTokenExists($token,$user))
            UserDevice::create([
                'user_id' => $user->id,
                'token' => $token
            ]);
    }

    public function deleteDevice(User $user, $token)
    {
        if (!$user->checkIfTokenExists($token))
            UserDevice::where(
                'user_id', $user->id
            )->where('token', $token)->first()->delete();
    }
}
