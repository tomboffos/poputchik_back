<?php


namespace App\Services;


class ImageService
{

    public static function storeImages($images)
    {
        $imagesArray = [];

        foreach ($images as $image){
            $imagesArray[] = is_file($image) ? $image->store('driver_profiles') : $image;
        }

        return json_encode($imagesArray);
    }
}
