<?php


namespace App\Services;


use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class FirebaseService
{
    public static function send($device_tokens, string $title, string $text)
    {
        $response = Http::withHeaders([
            'Authorization' => "key=" . env('PUSH_NOTIFICATION'),
            'Content-Type' => 'application/json',
        ])->post('https://fcm.googleapis.com/fcm/send', [
            "registration_ids" => $device_tokens,
            'notification' => [
                'title' => $title,
                'body' => $text,
                "sound" => 'notification.wav',
            ],
        ]);
//        Log::error('logging '.$response->);
        Log::info('message '.json_encode($response->body()));
        return $response->successful();
    }

}
