<?php


namespace App\Services;


class MessageContentService
{
    public static function processMessage($message)
    {
        $imagesArray = [];
        if (isset($message['images']))
            foreach ($message['images'] as $image) {
                $imagesArray[] = is_file($image) ? asset(str_replace('public/','','storage/'.$image->store('public/driver_profiles'))) : $image;
            }

        return json_encode([
            'images' => $imagesArray,
            'text' => $message['text']
        ]);
    }
}
