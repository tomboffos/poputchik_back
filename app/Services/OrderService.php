<?php


namespace App\Services;


use App\Models\Order;
use App\Models\OrderRequest;

class OrderService
{
    public function searchForOrders(Order $order)
    {
        foreach (OrderRequest::where('track_id', $order->track_id)->whereNull('order_id')->where('order_request_status_id', 1)
                     ->orderBy('id','desc')->get() as $requests) {
            if (count($order->requests()) < $order->seats) {
                $requests->update([
                    'order_request_status_id' => $order->automated ? 2 : 5,
                    'order_id' => $order->id
                ]);
            } else {
                break;
            }
        }
    }
}
