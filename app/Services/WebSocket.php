<?php


namespace App\Services;


use Illuminate\Support\Facades\Http;

class WebSocket
{
    public static function sendEvent($event, $data)
    {
         Http::post('https://socket.it-lead.net',[
            'event' => $event,
            'data' => $data
        ]);
    }

}
