<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TrackRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TrackCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TrackCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Track::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/track');
        CRUD::setEntityNameStrings('Маршрут', 'Маршруты');
        $this->crud->setCreateView('admin.track.track');
        $this->crud->setEditView('admin.track.edit');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('start_name')->label('Стартовая точка');
        CRUD::column('end_name')->label('Конечная точка');
        CRUD::column('start_price')->label('Стартовая цена');
        CRUD::column('start_price_comfort')->label('Стартовая цена комфорт');
//        CRUD::column('custom_price')->label('Произвольная цена');
//        CRUD::column('prices')->
//
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(TrackRequest::class);

        CRUD::field('start_name')->label('Стартовая точка');
        CRUD::field('end_name')->label('Конечная точка');
        CRUD::field('points')->label('Маршрут')->type('hidden');
        CRUD::field('start_price')->label('Стартовая цена');
        CRUD::field('start_price_comfort')->label('Стартовая цена комфорт');
        CRUD::field('custom_price')->label('Произвольная цена цена')->type('hidden')->value(0);
        CRUD::field('prices')->label('Произвольная цена')->type('repeatable')->fields([
            [
                'name' => 'kilometers',
                'type' => 'number',
                'label' => 'Километраж',
            ],
            [
                'name' => 'price',
                'type' => 'number',
                'label' => 'Цена',
            ]
        ]);
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
