<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SupportStoreMessageRequest;
use App\Http\Resources\ChatResource;
use App\Models\Message;
use App\Http\Requests\StoreMessageRequest;
use App\Http\Requests\UpdateMessageRequest;
use App\Repositories\MessageRepository;
use Illuminate\Http\Request;

class SupportChatController extends Controller
{
    private $messageRepository;

    public function __construct()
    {
        $this->messageRepository = new MessageRepository();
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index(Request $request)
    {
        //
        return view('admin.chat.index', [
            'chats' => Message::distinct('user_id')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store(SupportStoreMessageRequest $request)
    {
        return response(new ChatResource($this->messageRepository->createBySupport($request)));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Message $message
     */
    public function show(Message $message, Request $request)
    {
        return view('admin.chat.show', [
            'chats' => ChatResource::collection(Message::where('user_id', $message->user_id)->orderBy('id', 'desc')->get())
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Message $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Models\Message $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Message $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }
}
