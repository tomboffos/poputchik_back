<?php

namespace App\Http\Controllers\Api\General;

use App\Http\Controllers\Controller;
use App\Models\HelpPhone;
use Illuminate\Http\Request;

class HelpPhoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return HelpPhone::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\HelpPhone $helpPhone
     * @return \Illuminate\Http\Response
     */
    public function show(HelpPhone $helpPhone)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\HelpPhone $helpPhone
     * @return \Illuminate\Http\Response
     */
    public function edit(HelpPhone $helpPhone)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\HelpPhone $helpPhone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HelpPhone $helpPhone)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\HelpPhone $helpPhone
     * @return \Illuminate\Http\Response
     */
    public function destroy(HelpPhone $helpPhone)
    {
        //
    }
}
