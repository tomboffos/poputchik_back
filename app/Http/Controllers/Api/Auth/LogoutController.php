<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\LogoutRequest;
use App\Services\DeviceService;
use Illuminate\Http\Request;

class LogoutController extends Controller
{
    //
    /**
     * @var DeviceService
     */
    private $deviceService;

    public function __construct()
    {
        $this->deviceService = new DeviceService();
    }

    public function logout(LogoutRequest $request)
    {
        $this->deviceService->deleteDevice($request->token, $request->user());

        return response([], 200);
    }
}
