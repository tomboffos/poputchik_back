<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderRequestResource;
use App\Models\OrderRequest;
use App\Http\Requests\StoreOrderRequestRequest;
use App\Http\Requests\UpdateOrderRequestRequest;
use App\Repositories\OrderRequestRepository;
use App\Services\OrderService;
use Illuminate\Http\Request;

class OrderRequestController extends Controller
{
    private $orderRequestRepository;
    /**
     * @var OrderService
     */
    private $orderService;

    public function __construct()
    {
        $this->orderRequestRepository = new OrderRequestRepository();
        $this->orderService = new OrderService();
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index(Request $request)
    {
        //
        return OrderRequestResource::collection($request->user()->orderRequests());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreOrderRequestRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrderRequestRequest $request)
    {
        //
        $orderRequest = $this->orderRequestRepository->create($request);

//        $this->orderService->searchForOrderRequest($orderRequest);

        return response(new OrderRequestResource($orderRequest), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\OrderRequest $orderRequest
     * @return \Illuminate\Http\Response
     */
    public function show(OrderRequest $orderRequest)
    {
        //
        return  response(new OrderRequestResource($orderRequest));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\OrderRequest $orderRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderRequest $orderRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateOrderRequestRequest $request
     * @param \App\Models\OrderRequest $orderRequest
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrderRequestRequest $request, OrderRequest $orderRequest)
    {
        //
        return response(new OrderRequestResource($this->orderRequestRepository->update($request, $orderRequest)));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\OrderRequest $orderRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderRequest $orderRequest)
    {
        //
    }
}
