<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Auth\LoginRequest;
use App\Http\Requests\Api\User\Auth\RegisterRequest;
use App\Http\Requests\AuthCheckCodeRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    //
    private $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('phone', $request->phone)->first();
        if ($user && Hash::check($request->password, $user->password))
            return response([
                'user' => new UserResource($user),
                'token' => $user->createToken(Str::random(10))->plainTextToken
            ], 200);
        return response([
            'message' => 'Не правильный логин или пароль'
        ], 400);
    }

    public function register(RegisterRequest $request)
    {
        $user = $this->userRepository->create($request);

        return response([
            'user' => new UserResource($user),
            'token' => $user->createToken(Str::random(10))->plainTextToken
        ], 201);
    }

    public function check(AuthCheckCodeRequest $request, User $user)
    {
        $user = $this->userRepository->check($request, $user);

        if (!$user)
            return response(['message' => 'Не верный код'], 400);

        return response([
            'user' => new UserResource($user),
            'token' => $user->createToken(Str::random(10))->plainTextToken
        ]);
    }
}
