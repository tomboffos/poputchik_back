<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Driver\Auth\AuthDriverRequest;
use App\Http\Requests\BecomeDriverRequest;
use App\Http\Resources\DriverResource;
use App\Http\Resources\UserResource;
use App\Models\Driver;
use App\Repositories\DriverRepository;
use App\Services\Paybox;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DriverController extends Controller
{
    private $driverRepository;

    public function __construct()
    {
        $this->driverRepository = new DriverRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Driver $driver
     * @return \Illuminate\Http\Response
     */
    public function show(Driver $driver)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Driver $driver
     * @return \Illuminate\Http\Response
     */
    public function edit(Driver $driver)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Driver $driver
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $request->user()->driver->update($request->all());

        return response(new UserResource($request->user()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Driver $driver
     * @return \Illuminate\Http\Response
     */
    public function destroy(Driver $driver)
    {
        //
    }

    public function fillBalance(Request $request)
    {
        $payment = new Paybox();

        $payment->setQuery([
            'pg_amount' => $request->balance,
            'pg_order_id' => (string)mt_rand(0, 5000),
            'pg_user_id' => (string)$request->user()->id,
            'pg_salt' => Str::random(10),
            'pg_result_url' => '/',
            'pg_success_url' => '/',
            'pg_fail_url' => '/',
            'pg_description' => 'Пополнение баланса '
        ]);

        return response([
            'payment' => $payment->paymentLink()
        ], 200);

    }

    public function become(BecomeDriverRequest $request)
    {
        $user = $this->driverRepository->becomeDriver($request);
        return response([
            'user' => new DriverResource($user),
        ], 200);
    }

    public function addRating(Driver $driver,Request $request)
    {
        $driver->update([
            'rating' => $request->rating + $driver->rating,
            'rating_count' => $driver->rating_count + 1
        ]);

        return response([], 200);
    }
}
