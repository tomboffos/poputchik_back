<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Repositories\OrderRepository;
use App\Services\WebSocket;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    private $orderRepository;

    public function __construct()
    {
        $this->orderRepository = new OrderRepository();
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index(Request $request)
    {
        //

        return OrderResource::collection(Order::where('driver_id', $request->user()->driver->id)->where(function ($query) use ($request) {
            if ($request->has('start_date') && $request->has('end_date'))
                $query->whereBetween('created_at', [$request->start_date, $request->end_date]);
        })->whereIn('order_status_id', ['3', '4'])->orderBy('id','desc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreOrderRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrderRequest $request)
    {
        //
        return response(new OrderResource($this->orderRepository->create($request)));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Order $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
        return response(new OrderResource($order));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Order $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateOrderRequest $request
     * @param \App\Models\Order $order
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrderRequest $request, Order $order)
    {
        //
        return response(new OrderResource($this->orderRepository->update($request, $order)));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Order $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }


    public function cancelOrder(Order $order)
    {
        $order->update([
            'order_status_id' => 4
        ]);

        return response([], 200);
    }


    public function socket(Request $request)
    {
        WebSocket::sendEvent($request->event, json_decode($request->data));
    }
}
