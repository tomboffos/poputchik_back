<?php

namespace App\Http\Controllers\Api\Driver\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Driver\Auth\AuthDriverRequest;
use App\Http\Requests\AuthCheckCodeRequest;
use App\Http\Resources\DriverResource;
use App\Models\User;
use App\Repositories\DriverRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AuthDriverController extends Controller
{
    //
    /**
     * @var DriverRepository
     */
    private $driverRepository;

    public function __construct()
    {
        $this->driverRepository = new DriverRepository();
    }

    public function register(AuthDriverRequest $request)
    {
        $user = $this->driverRepository->create($request);
        return response([
            'user' => new DriverResource($user),
            'token' => $user->createToken(Str::random(10))->plainTextToken
        ], 201);
    }

    public function check(AuthCheckCodeRequest $request, User $user)
    {
        $user = $this->driverRepository->checkDriverCode($request, $user);

        if (!$user)
            return response(['message' => 'Не верный код'], 400);

        return response([
            'user' => new DriverResource($user),
            'token' => $user->createToken(Str::random(10))->plainTextToken
        ]);
    }
}
