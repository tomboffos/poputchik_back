<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'id' => $this->id,
            'surname' => $this->surname,
            'driver' => $this->driver,
            'phone' => $this->phone,
            'role_id' => $this->role_id,
            'notification_enabled' => $this->notification_enabled,
            'avatar' => $this->avatar,
            'rating' => $this->rating_count == 0  ? 0 : $this->rating / $this->rating_count

        ];
    }
}
