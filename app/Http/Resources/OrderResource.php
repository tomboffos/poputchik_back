<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'track' => $this->track,
            'comments' => $this->comments,
            'seats' => $this->seats,
            'automated' => $this->automated,
            'id' => $this->id,
            'driver' => new DriverUserResource($this->driver),
            'commission' => $this->commission,
            'price' => $this->price,
            'order_status' => $this->status,
            'requests' => OrderRequestSocket::collection($this->requests())
        ];
    }
}
