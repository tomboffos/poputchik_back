<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'user' => $this->user,
            'support' => $this->support,
            'message_content' => $this->message_content,
            'is_mine' => $request->user() != null ? $request->user()->id == $this->sender_id : backpack_user()->id == $this->sender_id
        ];
    }
}
