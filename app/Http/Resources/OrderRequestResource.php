<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'track' => $this->track,
            'price' => $this->price,
            'order_request_status' => $this->orderRequestStatus,
            'seats' => $this->seats,
            'comfort' => $this->comfort,
            'comments' => $this->comments,
            'created_at' => $this->created_at,
            'id' => $this->id,
            'order' => $this->order_id != null ? new OrderResource($this->order) : null
        ];
    }
}
