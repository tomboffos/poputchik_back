<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DriverUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'car_model' => $this->car_model,
            'car_color' => $this->car_color,
            'car_year' => $this->car_year,
            'name' => $this->user->name,
            'car_id' => $this->car_id,
            'phone' => $this->user->phone,
            'rating' => $this->rating_count == 0  ? 0 : $this->rating / $this->rating_count
        ];
    }
}
