<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'surname' => $this->surname,
            'phone' => $this->phone,
            'id' => $this->id,
            'role_id' => $this->role_id,
            'notification_enabled' => $this->notification_enabled,
            'avatar' => $this->avatar,
            'code' => $this->code,
            'rate' => $this->rate_count ? $this->rate / $this->rate_count : 0
        ];
    }
}
