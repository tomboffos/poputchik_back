<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'track_id' => 'required|exists:tracks,id',
            'seats' => 'required',
            'comfort' => 'required',
            'comments' => 'nullable',
            'payment_type_id' => 'required|exists:payment_types,id',
            'price' => 'required'
        ];
    }
}
