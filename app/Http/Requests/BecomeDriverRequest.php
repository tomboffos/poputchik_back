<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BecomeDriverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'car_color' => 'required',
            'car_year' => 'required',
            'car_model' => 'required',
            'car_id' => 'required',
            'iin' => 'required',
            'seats' => 'required|numeric',
            'driver_id_images' => 'required',
            'images' => 'required',
        ];
    }
}
