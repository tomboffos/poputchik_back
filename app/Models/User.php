<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'surname',
        'phone',
        'password',
        'role_id',
        'notification_enabled',
        'avatar',
        'code',
        'rate',
        'rate_count',
        'email',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function devices()
    {
        return $this->hasMany(UserDevice::class)->pluck('token');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function checkIfTokenExists($token, $user)
    {
        return UserDevice::where('user_id', $user->id)->where('token', $token)->exists();
    }

    public function driver()
    {
        return $this->hasOne(Driver::class);
    }

    public function orderRequests()
    {
        return $this->hasMany(OrderRequest::class)->whereIn(
            'order_request_status_id', ['2', '8']
        )->orderBy('id', 'desc')->get();
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function supportMessages()
    {
        return $this->hasMany(Message::class, 'support_id', 'user_id');
    }


    public function setAvatarAttribute($value)
    {
        $this->attributes['avatar'] = asset('/storage/' . str_replace('public/', '', $value->store('public/avatars')));
    }


}
