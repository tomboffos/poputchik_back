<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderRequest extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'order_id',
        'user_id',
        'track_id',
        'order_request_status_id',
        'price',
        'seats',
        'comfort',
        'comments',
        'payment_type_id'
    ];

    public function track()
    {
        return $this->belongsTo(Track::class);
    }

    public function orderRequestStatus()
    {
        return $this->belongsTo(OrderRequestStatus::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
