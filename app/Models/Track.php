<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Track extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'start_name',
        'end_name',
        'points',
        'start_price',
        'start_price_comfort',
        'custom_price',
        'prices'
    ];

    protected $casts = [
        'prices' => 'array'
    ];

    public function preparingOrders()
    {
        return $this->hasMany(Order::class)->where('order_status_id', 1);
    }
}
