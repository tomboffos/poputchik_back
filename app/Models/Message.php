<?php

namespace App\Models;

use App\Services\MessageContentService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id', 'support_id', 'message_content', 'sender_id'
    ];


    public function setMessageContentAttribute($value)
    {
        $this->attributes['message_content'] = MessageContentService::processMessage($value);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function support()
    {
        return $this->belongsTo(User::class);
    }

    public function getMessageContentAttribute()
    {
        return json_decode($this->attributes['message_content']);
    }
}
