<?php

namespace App\Models;

use App\Services\ImageService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Driver extends Model
{
    use HasFactory, SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class)->whereIn('order_status_id', ['3', '4'])->get();
    }

    protected $fillable = [
        'user_id',
        'car_id',
        'car_color',
        'car_model',
        'car_year',
        'iin',
        'seats',
        'driver_id_images',
        'images',
        'balance',
        'is_active',
        'rating',
        'rating_count'
    ];

    public function setDriverIdImagesAttribute($value)
    {
        $this->attributes['driver_id_images'] = ImageService::storeImages($value);
    }

    public function setImagesAttribute($value)
    {
        $this->attributes['images'] = ImageService::storeImages($value);
    }
}
