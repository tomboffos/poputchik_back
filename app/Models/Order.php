<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'track_id',
        'driver_id',
        'comments',
        'seats',
        'automated',
        'commission',
        'price',
        'order_status_id'
    ];

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function track()
    {
        return $this->belongsTo(Track::class);
    }

    public function status()
    {
        return $this->belongsTo(OrderStatus::class);
    }

    public function requests()
    {
        return $this->hasMany(OrderRequest::class)->whereNotIn('order_request_status_id', ['4', '3'])->get();
    }

    public function completedRequest() : HasMany
    {
        return $this->hasMany(OrderRequest::class)->where('order_request_status_id','8');
    }
}


