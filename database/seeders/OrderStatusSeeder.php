<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('order_statuses')->insert([
           'id' => 1,
           'name' => 'Начало заказа',
        ]);

        DB::table('order_statuses')->insert([
            'id' => 2,
            'name' => 'Заказ выполняется',
        ]);

        DB::table('order_statuses')->insert([
            'id' => 3,
            'name' => 'Заказ завершин',
        ]);

        DB::table('order_statuses')->insert([
            'id' => 4,
            'name' => 'Заказ отменен водителем',
        ]);
    }
}
