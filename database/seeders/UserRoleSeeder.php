<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insert([
            'name' => 'Пользователь',
            'id' => 1
        ]);

        DB::table('roles')->insert([
            'name' => 'Водитель',
            'id' => 2
        ]);
    }
}
