<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderRequestStatusAdditionalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('order_request_statuses')->insert([
            'id' => 5,
            'name' => 'Предложено водителю'
        ]);
    }
}
