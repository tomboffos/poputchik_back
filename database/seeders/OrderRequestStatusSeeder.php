<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderRequestStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('order_request_statuses')->insert([
           'id' => 1,
           'name' => 'В поиске водителя'
        ]);

        DB::table('order_request_statuses')->insert([
            'id' => 2,
            'name' => 'Водитель принял'
        ]);

        DB::table('order_request_statuses')->insert([
            'id' => 3,
            'name' => 'Водитель отказал'
        ]);
    }
}
