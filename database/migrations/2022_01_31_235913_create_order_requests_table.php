<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_requests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_id')->nullable()->constrained();
            $table->foreignId('track_id')->constrained();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('order_request_status_id')->constrained();
            $table->unsignedBigInteger('price');
            $table->unsignedBigInteger('seats');
            $table->boolean('comfort');
            $table->text('comments')->nullable();
            $table->foreignId('payment_type_id')->constrained();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_requests');
    }
}
