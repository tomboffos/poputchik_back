<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('track') }}'><i class='nav-icon la la-route'></i> Маршруты</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('help-phone') }}'><i class='nav-icon la la-phone'></i> Телефоны экстренного вызова</a></li>
<li class='nav-item'><a class='nav-link' href='/messages'><i class='nav-icon la la-message'></i>Сообщение</a></li>
