@extends(backpack_view('blank'))

@php
    $defaultBreadcrumbs = [
      trans('backpack::crud.admin') => backpack_url('dashboard'),
      $crud->entity_name_plural => url($crud->route),
      trans('backpack::crud.edit') => false,
    ];

    // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
    $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
@endphp

@section('header')
    <section class="container-fluid">
        <h2>
            <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
            <small>{!! $crud->getSubheading() ?? trans('backpack::crud.edit').' '.$crud->entity_name !!}.</small>

            @if ($crud->hasAccess('list'))
                <small><a href="{{ url($crud->route) }}" class="d-print-none font-sm"><i class="la la-angle-double-{{ config('backpack.base.html_direction') == 'rtl' ? 'right' : 'left' }}"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
            @endif
        </h2>
    </section>
@endsection

@section('content')
    <div class="row">
        <div class="{{ $crud->getEditContentClass() }}">
            <!-- Default box -->

            @include('crud::inc.grouped_errors')

            <form method="post"
                  action="{{ url($crud->route.'/'.$entry->getKey()) }}"
                  @if ($crud->hasUploadFields('update', $entry->getKey()))
                      enctype="multipart/form-data"
                @endif
            >
                {!! csrf_field() !!}
                {!! method_field('PUT') !!}
                <div id="map" style="width: 100%;height: 400px;"></div>

                @if ($crud->model->translationEnabled())
                    <div class="mb-2 text-right">
                        <!-- Single button -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{trans('backpack::crud.language')}}: {{ $crud->model->getAvailableLocales()[request()->input('locale')?request()->input('locale'):App::getLocale()] }} &nbsp; <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                @foreach ($crud->model->getAvailableLocales() as $key => $locale)
                                    <a class="dropdown-item" href="{{ url($crud->route.'/'.$entry->getKey().'/edit') }}?locale={{ $key }}">{{ $locale }}</a>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
                <!-- load the view from the application if it exists, otherwise load the one in the package -->
                @if(view()->exists('vendor.backpack.crud.form_content'))
                    @include('vendor.backpack.crud.form_content', ['fields' => $crud->fields(), 'action' => 'edit'])
                @else
                    @include('crud::form_content', ['fields' => $crud->fields(), 'action' => 'edit'])
                @endif

                @include('crud::inc.form_save_buttons')
            </form>
        </div>
    </div>

    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGsNhQ_o5KNOtcSVmAsuh7GeGGkkaYT7c"
    ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"
            integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.1.2/axios.min.js"
            integrity="sha512-bHeT+z+n8rh9CKrSrbyfbINxu7gsBmSHlDCb3gUF1BjmjDzKhoKspyB71k0CIRBSjE5IVQiMMVBgCWjF60qsvA=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        function initMap(currentPosition) {

            const uluru = {lat: currentPosition.coords.latitude, lng: currentPosition.coords.longitude};
            // The map, centered at Uluru
            const map = new google.maps.Map(document.getElementById("map"), {
                zoom: 15,
                center: uluru,
            });


            map.addListener('click', (e) => {
                addPolygons(e.latLng, map)

            })
        }

        let polylines = []
        let fullPolyline = []
        let polygon = new google.maps.Polyline({
            paths: polylines,
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#FF0000",
            fillOpacity: 0.35,
            map
        })

        function addPolygons(latLng, map) {

            if (polylines.length) {

                const directionsService = new google.maps.DirectionsService();
                directionsService.route({
                    origin: `${polylines[0].lat},${polylines[0].lng}`,
                    waypoints: polylines.map((e) => ({
                        location: `${e.lat},${e.lng}`,
                        stopover: false
                    })),
                    destination: `${latLng.toJSON().lat},${latLng.toJSON().lng}`,
                    travelMode: "DRIVING",
                    optimizeWaypoints: true,

                }, (response, status) => {

                    directionDisplay = new google.maps.DirectionsRenderer();
                    directionDisplay.setMap(null);
                    directionDisplay.suppressMarkers = true;
                    directionDisplay.setMap(map);
                    directionDisplay.setDirections(response)


                    fullPolyline = response.routes[0].overview_path.map((element) => {
                        return element.toJSON()
                    })
                    let areaField = document.querySelectorAll('input[name="points"]')

                    areaField[0].value = JSON.stringify(fullPolyline.map((latLng) => [
                        latLng.lat,
                        latLng.lng
                    ]))

                });

            }


            polylines.push(latLng.toJSON())
            polygon.setMap(null)

            // setField()
        }

        function setField() {
            let areaField = document.querySelectorAll('input[name="points"]')
            areaField[0].value = JSON.stringify(fullPolyline.map((latLng) => [
                latLng.lat,
                latLng.lng
            ]))
        }

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(initMap);
        }

    </script>
@endsection

