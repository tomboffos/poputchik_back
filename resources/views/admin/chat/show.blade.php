@extends(backpack_view('blank'))

@section('header')
    <section class="container-fluid">

    </section>

@endsection
@section('content')
    <div class="container">
        <div class=""
             id="chat-container"
             style="height: 500px;background-color: #fff;display: flex;flex-direction: column-reverse;overflow-y: scroll;">
            @foreach($chats as $chat)
                <div class="alert-primary"
                     style="padding: 20px;margin: 20px;border-radius: 20px;{{$chat['is_mine'] ? 'align-self:flex-start;' : 'align-self:flex-end;'}}">
                    @if(isset($chat['message_content']->images))
                        @foreach($chat['message_content']->images as $image)
                            <img src="{{$image}}" style="width: 100px;height: 100px" alt="">
                        @endforeach
                    @endif
                    @if(isset($chat['message_content']->text))
                        {{$chat['message_content']->text}}

                    @endif
                </div>
            @endforeach
        </div>
        <div class="d-flex">
            <input type="text" class="w-100" name="w-100" id="message">
            <button type="submit" class="btn-primary" onclick="sendMessage()">Отправить</button>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.25.0/axios.min.js"
                integrity="sha512-/Q6t3CASm04EliI1QyIDAA/nDo9R8FQ/BULoUFyN4n/BDdyIxeH7u++Z+eobdmr11gG5D/6nPFyDlnisDwhpYA=="
                crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
        <script>
            const socket = io('https://socket.it-lead.net');
            socket.on("connect", () => {
                console.log(socket.id);
            });
            socket.io.on("error", (error) => {
                console.log(error.message)
            });
            socket.on('poputchik.message.sent.{{$chats[0]->user_id}}', (data) => {
                document.getElementById('chat-container').innerHTML = `
                 <div class="alert-primary"
                     style="padding: 20px;margin: 20px;border-radius: 20px;${data.is_mine ? 'align-self:flex-start;' : 'align-self:flex-end;'}">
                   ${data.message_content.text != null ? data.message_content.text : ''}
                </div>
                ${document.getElementById('chat-container').innerHTML}
                `
            })

            function sendMessage() {
                axios.post('/messages', {
                    'message_content': {
                        'text': document.getElementById('message').value,
                        'images': []
                    },
                    'user_id':{{$chats[0]->user_id}}

                }).then((value) => document.getElementById('message').value = '')
            }
        </script>

    </div>
@endsection
