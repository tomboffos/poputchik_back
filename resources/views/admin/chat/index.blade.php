@extends(backpack_view('blank'))

@section('header')
    <section class="container-fluid">

    </section>

@endsection
@section('content')
    <div class="row">
        @foreach($chats as $chat)
            <div class="container bg-primary d-flex justify-content-between" style="padding:20px;border-radius: 10px;">
                <a href="/messages/{{$chat->id}}" style="color: #fff;">

                    <p style="font-size: 20px;">
                        {{$chat->user->name}}
                    </p>

{{--                    <p style="font-size: 20px;">--}}
{{--                        {{$chat->message_content}}--}}
{{--                    </p>--}}
                </a>

            </div>
        @endforeach
    </div>
@endsection
